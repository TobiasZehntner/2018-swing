# Swing (2018)

Artwork by [Tobias Zehntner](http://www.tobiaszehntner.art)

- Artwork ©2018 Tobias Zehntner
- Code licensed as Open Source: [APGL-3.0](https://www.gnu.org/licenses/agpl.html)
- [Documentation](http://www.tobiaszehntner.art/work/swingaarau)

### Technical

#### Motor Unit
RaspberryPi Mini controlling actuator, to swing the light bulb like a pendulum.

#### Light Unit
Accelerometer > RaspberryPi Mini > Arduino > AC Dimmer > Bulb
Light bulb is burning brighter the faster it moves.

### Artwork
As a new iteration of the work [*Swing* (2014)](http://www.tobiaszehntner.art/work/swing), the swinging light bulb was set up for the collaborative installation and performance *Multiplication of Rhythm* at the Alte Reithalle in Aarau, Switzerland. Its oscillating rhythm served as a starting point for participating artists, musicians and dancers to develop a performative exhibition.
