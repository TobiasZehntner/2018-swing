# Make this file executable with `chmod 755 launch_light.sh`
# Add it to cron: `sudo crontab -e`
# @reboot sh /home/pi/swing/code/motor_unit/launch_motor.sh >/home/pi/swing/code/logs/cronlog 2>&1

sudo /usr/bin/python /home/pi/swing/code/motor_unit/main.py
