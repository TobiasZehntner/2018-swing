# -*- coding: utf-8 -*-
# © 2018 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import time
from math import sqrt

import Adafruit_ADS1x15
import RPi.GPIO as GPIO

# Pendulum frequency
# PENDULUM_LENGTH = 12  # Length in meters
# DT = 6.28 * sqrt(PENDULUM_LENGTH / 9.82) / 2  # one swing in secs, about 3.47
DT = 3.4
MOTOR_DT = 3.0  # Seconds the motor runs in one direction (has to be below DT)

# Analog to Digital Converter
ADC_GAIN = 2
ADC_CHANNEL = 0

# Motor
PIN_MOTOR1 = 6
PIN_MOTOR2 = 13
PIN_MOTOR3 = 19
PIN_MOTOR4 = 26

PWM_HZ = 500  # 50HZ at above 50% speed, sliding down to 20Hz at 1% speed

# Settings
MAX_ADC = 32800
MID_ADC = 26600
MIN_ADC = 16600
MIN_ANG = -45
MAX_ANG = 30

ACC_ANG = 10  # Degrees to ramp up/down for
MIN_SPEED = 20  # Minimum speed when start/finish ramping up/down (> 0)

FORWARD_ANGLE = 30
BACKWARD_ANGLE = -40

WITH_MOTOR_INTERVALS = False
MOTOR_INTERVALS_ON = 50  # Number of intervals the motor runs (18/minute)
MOTOR_INTERVALS_OFF = 6  # Number of intervals the motor is off (even number)


def mapping(x, in_min, in_max, out_min, out_max):
    """
    Arduino's map() function. Re-maps a number from one range to another
    """
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def setup():
    """
    Set up GPIO pins for Motor and Analog to Digital Converter (ADC)
    Motor diagram
                pin1    pin2    pin3    pin4
    forward     1(pwm)  0       0       1(pwm)
    backward    0       1(pwm)  1(pwm)  0
    brake       1       1       0       0
    brake       0       0       1       1
    /!\ burn    1       0       1       0
    H-Bridge    0       1       0       1
    """
    global adc, pwm1, pwm2, pwm3, pwm4
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # Set ADC
    GPIO.setup(ADC_CHANNEL, GPIO.IN)
    adc = Adafruit_ADS1x15.ADS1115()
    adc.start_adc(ADC_CHANNEL, gain=ADC_GAIN)

    # Set Motor PWM
    GPIO.setup([
        PIN_MOTOR1,
        PIN_MOTOR2,
        PIN_MOTOR3,
        PIN_MOTOR4,
    ], GPIO.OUT)

    pwm1 = GPIO.PWM(PIN_MOTOR1, PWM_HZ)
    pwm2 = GPIO.PWM(PIN_MOTOR2, PWM_HZ)
    pwm3 = GPIO.PWM(PIN_MOTOR3, PWM_HZ)
    pwm4 = GPIO.PWM(PIN_MOTOR4, PWM_HZ)

    # Set Motor to brake at start
    pwm1.start(100)
    pwm2.start(100)
    pwm3.start(0)
    pwm4.start(0)


def forward(speed):
    """
    Move motor forward at chosen speed
    :param speed: 0-100
    :type speed: int
    """
    pwm1.ChangeDutyCycle(speed)
    pwm2.ChangeDutyCycle(0)
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(speed)


def backward(speed):
    """
    Move motor backwards at chosen speed
    :param speed: 0-100
    :type speed: int
    """
    pwm1.ChangeDutyCycle(0)
    pwm2.ChangeDutyCycle(speed)
    pwm3.ChangeDutyCycle(speed)
    pwm4.ChangeDutyCycle(0)


def brake():
    """
    Set motor to break
    """
    pwm1.ChangeDutyCycle(100)
    pwm2.ChangeDutyCycle(100)
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(0)


def stop():
    """
    Stop motor and GPIO signals
    """
    brake()
    pwm1.stop()
    pwm2.stop()
    pwm3.stop()
    pwm4.stop()


def get_angle():
    """
    :return: Approx angle of swing arm
    :rtype: float
    """
    adc_reading = adc.get_last_result()
    if adc_reading <= MID_ADC:
        angle = mapping(
            float(adc.get_last_result()), MIN_ADC, MID_ADC, MIN_ANG, 0)
    else:
        angle = mapping(
            float(adc.get_last_result()), MID_ADC, MAX_ADC, 0, MAX_ANG)
    return float('%.2f' % angle)


def go_to_angle(target_angle, target_time=DT):
    """
    Move to target angle with pre-defined ramp-up/down of motor
    within target_time. Abort at end of target_time.
    :param target_time: The time to reach the target_angle
    :type target_time: float
    :param target_angle: Target angle
    :type target_angle: float
    """
    current_angle = get_angle()
    start_angle = current_angle
    start_time = time.time()
    # Aim for precision of 0.2 degrees
    while abs(current_angle - target_angle) > 0.2:
        current_angle = get_angle()
        end_diff = abs(current_angle - target_angle)
        start_diff = abs(current_angle - start_angle)

        if start_diff < ACC_ANG:
            speed = mapping(start_diff, 0, ACC_ANG, MIN_SPEED, 100)
        elif end_diff < ACC_ANG:
            speed = mapping(end_diff, 0, ACC_ANG, MIN_SPEED, 100)
        else:
            speed = 100

        if current_angle > target_angle:
            backward(speed)
        else:
            forward(speed)
            
        # Cancel going to target_angle if we're passed target_time
        if time.time() > (start_time + target_time):
            print 'Passed target time, abort go_to_angle'
            break
        
    brake()


def main():
    """
    Running setup and then the main loop
    """
    try:
        setup()

        go_forward = False
        counter = 0  # Number of DT loops
        start_time = time.time()
        while True:
            counter += 1
            
            if go_forward:
                forward(100)
                go_forward = False
            else:
                backward(100)
                go_forward = True

            # Run motor in one direction for MOTOR_DT duration
            target_dt_time = start_time + (DT * counter)
            motor_dt_time = target_dt_time - DT + MOTOR_DT
            motor_sleep = motor_dt_time - time.time()
            if motor_sleep > 0:
                time.sleep(motor_sleep)
            brake()

            # Run loop exactly every DT
            sleep = target_dt_time - time.time()
            if sleep > 0:
                time.sleep(sleep)

    except KeyboardInterrupt:
        print '\nExiting...'
    except Exception, e:
        print 'Error: %s' % e
    finally:
        print '\nShutting down...'
        stop()
        adc.stop_adc()
        GPIO.cleanup()
        print 'Done.'


if __name__ == '__main__':
    # execute only if run as a script
    main()
