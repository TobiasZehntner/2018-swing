# Make this file executable with `chmod 755 launcher.sh`
# Add it to cron: `sudo crontab -e`
# @reboot sh /home/pi/swing/code/light_unit/launch_light.sh >/home/pi/swing/code/logs/cronlog 2>&1

sudo /usr/bin/python /home/pi/swing/code/light_unit/main.py
