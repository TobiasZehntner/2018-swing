// -*- coding: utf-8 -*-
// © 2018 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// LIBRARIES
#include <TimerOne.h>
#include "Arduino.h"
#include <digitalWriteFast.h>
#include <Wire.h>
#define SLAVE_ADDRESS 0x04
int number = 0;
int state = 0;

const byte PIN_INTERRUPT = 2;           // Interrupt 0 = Pin 2 Input from Dimmer
const byte PIN_DIM_OUT = 9;             // Output to Opto Triac
byte dim = 128;                  // Dimming level (0-128)  0 = on, 128 = 0ff           
volatile int i = 0;               // Variable to use as a counter            
volatile boolean zero_cross = 0;  // Boolean to store a "switch" to tell us if we have crossed zero
int freqStep = 78;                // This is the delay-per-brightness step in microseconds.

void zero_cross_detect() {
  // Turning off Triac at Zero crossing and start timer
  zero_cross = true;               
  i=0;
  digitalWrite(PIN_DIM_OUT, LOW);
}  

void dim_check() {
  // Turning on Triac between two zero-crossings after proportional time
  // regarding to dim level: 0, we turn it on right away, 128, we won't turn it on at all
  // This function gets called 128 times between each crossing
  if(zero_cross == true) {      
    if(i >= dim) {                     
      digitalWrite(PIN_DIM_OUT, HIGH);
    }  
    i++;                                                      
  }                                  
}   

void receiveData(int byteCount){
  while(Wire.available()) {
    dim = Wire.read();
    Serial.println(dim);
  }
}

// callback for sending data
void sendData(){
  Wire.write(dim);
}

void setup() {                      
  pinMode(PIN_DIM_OUT, OUTPUT);
  attachInterrupt(PIN_INTERRUPT, zero_cross_detect, RISING);
  Timer1.initialize(freqStep);
  Timer1.attachInterrupt(dim_check, freqStep); 

  pinMode(13, OUTPUT);
  Serial.begin(9600); // start serial for output
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);
  
  Serial.println("Ready!");
}

void loop() {

}

