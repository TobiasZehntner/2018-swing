# -*- coding: utf-8 -*-
# © 2018 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import smbus
import sys
import time
from math import sqrt

# Add specific path so cron finds mpu6050 and numpy
sys.path.append('/home/pi/.local/lib/python2.7/site-packages/')
from mpu6050 import mpu6050
from numpy import cumsum as np_cumsum, insert as np_insert

ARDUINO_ADDRESS = 0x04
ACCELERATOR_ADDRESS = 0x68

AVG_NUM = 10  # Size of array for rolling average

FS = 50  # Readings per second
DT = 1.0 / FS

MAX_DIM = 105  # 128 = dark. Flicker below 115
MIN_DIM = 40  # 0 = full brightness, 30 ok


def norm(v):
    """
    Return acceleration vector from xyz dict
    """
    return sqrt(v['x'] ** 2 + v['y'] ** 2 + v['z'] ** 2)


def running_mean(x, N):
    """
    Rolling average of acceleration data
    """
    cumsum = np_cumsum(np_insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)


def mapping(x, in_min, in_max, out_min, out_max):
    """
    Arduino's map() function: Re-maps a number from one range to another
    """
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def main():
    """
    Running setup and then the main loop
    """
    try:
        sensor = mpu6050(ACCELERATOR_ADDRESS)
        arduino = smbus.SMBus(1)

        # Initial acceleration data
        max_acc = 12.2
        min_acc = 7.7
        average_list = [9.3 for num in xrange(AVG_NUM)]

        counter = 0
        start_time = time.time()
        while True:
            # Average the accelerometer data over the past AVG_NUM readings
            average_list.append(norm(sensor.get_accel_data()))
            average_list.pop(0)
            acc_vec = running_mean(average_list, AVG_NUM)[0]

            # Map acceleration vector to dim amount and send it to Arduino
            dim = int(mapping(acc_vec, min_acc, max_acc, MAX_DIM, MIN_DIM))
            arduino.write_byte(ARDUINO_ADDRESS, dim)

            # Save max/min amount of acceleration to adapt dimming range
            if acc_vec > max_acc:
                max_acc = acc_vec
                print 'Max Acceleration: %s' % max_acc
            if acc_vec < min_acc:
                min_acc = acc_vec
                print 'Min Acceleration: %s' % min_acc

            # Aim for precise interval
            counter += 1
            sleep = start_time + (DT * counter) - time.time()
            if sleep > 0:
                time.sleep(sleep)
            else:
                print 'Too slow: loop took longer than DT. Reduce f/s.'

    except KeyboardInterrupt:
        print '\nExiting...'
    except Exception, e:
        print 'Error: %s' % e
    finally:
        print '\nShutting down...'
        # Turn off dimmer
        arduino.write_byte(ARDUINO_ADDRESS, 128)
        print 'Done.'


if __name__ == '__main__':
    # execute only if run as a script
    main()
